<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class tooleht extends Model
{
    use HasFactory;
    public function children(){
        return $this->hasMany(Tooleht::Class, 'parent_id')->with('children');
    }
}
?>