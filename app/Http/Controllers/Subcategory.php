<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Subcategory extends Controller
{
    public function index()
    {
        $categories = Category::with('subcategories')->get();
        
        return inertia('Tooleht', compact('categories'));
    }
}
