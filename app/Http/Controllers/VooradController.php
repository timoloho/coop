<?php

namespace App\Http\Controllers;

use App\Models\voorad;
use Illuminate\Http\Request;
use Inertia\Inertia;

class VooradController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return Inertia::render('Voorad/index', [
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(voorad $voorad)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(voorad $voorad)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, voorad $voorad)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(voorad $voorad)
    {
        //
    }
}
