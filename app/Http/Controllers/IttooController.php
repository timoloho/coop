<?php

namespace App\Http\Controllers;

use App\Models\ittoo;
use Illuminate\Http\Request;
use Inertia\Inertia;

class IttooController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return Inertia::render('Ittoo/index', [
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(ittoo $ittoo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(ittoo $ittoo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, ittoo $ittoo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(ittoo $ittoo)
    {
        //
    }
}
