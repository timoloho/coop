<?php

namespace App\Http\Controllers;

use App\Models\tooleht;
use Illuminate\Http\Request;
use Inertia\Inertia;

class ToolehtController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return Inertia::render('Tooleht/index', [
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return Inertia::render(
            'Tooleht/Create'
        );
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     */
    public function show(tooleht $tooleht)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(tooleht $tooleht)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, tooleht $tooleht)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(tooleht $tooleht)
    {
        //
    }
}
