<?php

use App\Http\Controllers\ProfileController;
use App\Http\Controllers\IttooController;
use App\Http\Controllers\ToomehedController;
use App\Http\Controllers\VooradController;
use App\Http\Controllers\ToolehtController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::get('/dashboard', function () {
    return Inertia::render('Dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');

    Route::resource('ittoo', IttooController::class);
    Route::resource('toomehed', ToomehedController::class);
    Route::resource('voorad', VooradController::class);
    Route::resource('tooleht', ToolehtController::class);
    Route::get('/list', 'ToolehtController@multiLevelCategory');
});

require __DIR__.'/auth.php';
